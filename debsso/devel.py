import os
from django.contrib.auth.middleware import RemoteUserMiddleware

class LocalUserMiddleware(RemoteUserMiddleware):

    def process_request(self, request):
        if 'REMOTE_USER' not in request.META:
            user = os.getenv('USER', 'nobody')
            domain = os.getenv('DOMAIN', 'example.com')
            request.META['REMOTE_USER'] = '@'.join((user, domain))
        return super().process_request(request)
